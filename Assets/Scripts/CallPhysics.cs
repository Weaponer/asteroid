﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Обработка физики
public enum PhysicsLayer
{
    Player,
    Asteroid,
    MyBullet
}

public class CallPhysics : MonoBehaviour
{
    static public CallPhysics Singleton;

    private List<IActorPhysics> actors = new List<IActorPhysics>();

    private void Awake()
    {
        if (Singleton)
        {
            Destroy(gameObject);
        }
        else
        {
            Singleton = this;
        }
    }

    public void AddActor(IActorPhysics actor)
    {
        actors.Add(actor);
    }

    public void RemoveActor(IActorPhysics actor)
    {
        actors.Remove(actor);
    }

    private void FixedUpdate()
    {
        UpdateMove();
        UpdateCollider();
    }

    private void UpdateMove()//Движение обьектов
    {
        for (int i = 0; i < actors.Count; i++)
        {
            Transform tr = actors[i].transform;
            tr.position += actors[i].Velocity;
            tr.Rotate(new Vector3(0, 0, actors[i].AngularVelocity));
            if (Mathf.Abs(tr.position.x) > GameBorders.Borders.x || Mathf.Abs(tr.position.y) > GameBorders.Borders.y)
            {
                tr.position = -tr.position;
            }

        }
    }

    private void UpdateCollider()//Проверить соприкасновение колайдеров
    {
        for (int i = 0; i < actors.Count; i++)
        {
            if (actors[i].Layer == PhysicsLayer.Player)
            {
                for (int i2 = 0; i2 < actors.Count; i2++)
                {
                    if (actors[i2].Layer == PhysicsLayer.Asteroid)
                    {
                        if (CheckCollider(actors[i], actors[i2]))
                        {
                            GameControll.Singleton.KillPlayer();
                            break;
                        }
                    }
                }
                break;
            }
        }

        for (int i = 0; i < actors.Count; i++)
        {
            if (actors[i].Layer == PhysicsLayer.Asteroid)
            {
                for (int i2 = 0; i2 < actors.Count; i2++)
                {
                    if (actors[i2].Layer == PhysicsLayer.MyBullet)
                    {
                        if (CheckCollider(actors[i], actors[i2]))
                        {
                            Asteroid ast = ((Asteroid)actors[i]);
                            BulletPlayer bullet = ((BulletPlayer)actors[i2]);
                            ast.DestroyBulletPlayer();
                            bullet.Destroy();
                            break;
                        }
                    }
                }
            }
        }
    }

    private bool CheckCollider(IActorPhysics actor1, IActorPhysics actor2)//Проверка на столкновение
    {
        float dist = Vector3.Distance(actor1.transform.position, actor2.transform.position);
        float radius = actor1.RadiusColider + actor2.RadiusColider;
        if (dist < radius)
        {
            return true;
        }
        return false;
    }

    private void OnDestroy()
    {
        Singleton = null;
    }
}
