﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBorders : MonoBehaviour 
{
    [SerializeField] private Camera camera;

    static public Vector2 Borders { get; private set; }// Размер игрового поля
    private void Awake()
    {
        Vector2 b;
        b.x = camera.orthographicSize * Screen.width / Screen.height;
        b.y = camera.orthographicSize;
        Borders = b;
    }
}
