﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IActorPhysics //Интерфейс физичиского обьекта
{
    PhysicsLayer Layer { get; set; }
    float RadiusColider { get; set; }
    Vector3 Velocity { get; set; }
    float AngularVelocity { get; set; }
    Transform transform { get; }
}
