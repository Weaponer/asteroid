﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPlayer : MonoBehaviour, IActorPhysics
{
    public PhysicsLayer Layer { get { return PhysicsLayer.MyBullet; } set { } }
    public float RadiusColider { get { return 0; } set { } }
    public Vector3 Velocity { get; set; }
    public float AngularVelocity { get; set; }

    private void Start()
    {
        CallPhysics.Singleton.AddActor(this);
        StartCoroutine(TimeLife());
    }

    IEnumerator TimeLife()//Время через которое пуля пропадет.
    {
        yield return new WaitForSeconds(1.7f);
        Destroy();
    }

    public void Destroy()//Уничтожить пулю
    {
        Destroy(gameObject);
    }

    private void OnDisable()
    {
        if (CallPhysics.Singleton)
            CallPhysics.Singleton.RemoveActor(this);
    }
}
