﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AsteroidType
{
    Big,
    Small
}

public delegate void PlayerDestroyAsteroid(Asteroid asteroid);

public class Asteroid : MonoBehaviour, IActorPhysics
{
    public event PlayerDestroyAsteroid DestroyBulletPlayerMy;

    public AsteroidType TypeSize;

    public float Radius;

    public Vector3 Velocity { get; set; }
    public float AngularVelocity { get; set; }
    public PhysicsLayer Layer { get { return PhysicsLayer.Asteroid; } set { } }
    public float RadiusColider { get { return Radius; } set { } }

    private void Start()
    {
        CallPhysics.Singleton.AddActor(this);
        if (TypeSize == AsteroidType.Big)
        {
            Velocity = new Vector3(Random.RandomRange(-0.05f, 0.05f), Random.RandomRange(-0.05f, 0.05f), 0f);
            AngularVelocity = Random.RandomRange(-2f, 2f);
        }
        else
        {
            Velocity = new Vector3(Random.RandomRange(-0.08f, 0.08f), Random.RandomRange(-0.08f, 0.08f), 0f);
            AngularVelocity = Random.RandomRange(-2f, 2f);
        }
    }

    public void DestroyBulletPlayer()//Игрок уничтожил астеройд
    {
        DestroyBulletPlayerMy(this);
        Destroy(gameObject);
    }

    private void OnDisable()
    {
        if (CallPhysics.Singleton)
            CallPhysics.Singleton.RemoveActor(this);
    }
}
