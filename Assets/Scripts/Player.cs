﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IActorPhysics
{
    public float Radius;

    [SerializeField] private BulletPlayer bulletPrefab;

    public PhysicsLayer Layer { get { return PhysicsLayer.Player; } set { } }
    public float RadiusColider { get { return Radius; } set { } }

    public Vector3 Velocity { get; set; }
    public float AngularVelocity { get; set; }

    private void Start()
    {
        CallPhysics.Singleton.AddActor(this);

    }

    private void LateUpdate()//Обновление управления корабля
    {
        Vector3 move = Velocity;
        if (Input.GetKey(KeyCode.W))
        {
            move.y += 0.0004f;
        }
        if (Input.GetKey(KeyCode.S))
        {
            move.y -= 0.0004f;
        }

        if (Input.GetKey(KeyCode.A))
        {
            move.x -= 0.0004f;
        }

        if (Input.GetKey(KeyCode.D))
        {
            move.x += 0.0004f;
        }

        move.y = Mathf.Clamp(move.y, -0.04f, 0.04f);
        move.x = Mathf.Clamp(move.x, -0.04f, 0.04f);
        Velocity = move;

        transform.eulerAngles = new Vector3(0, 0, GetAngle());

        if (Input.GetMouseButtonDown(0))
        {
            Fire();
        }
    }

    private void Fire()//Сделать выстрел
    {
        BulletPlayer bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        bullet.Velocity = transform.up / 10f;
    }

    private float GetAngle()//Получить угол направления 
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.x /= Screen.width;
        mousePos.y /= Screen.height;

        mousePos.x = (mousePos.x * GameBorders.Borders.x * 2) - GameBorders.Borders.x;
        mousePos.y = (mousePos.y * GameBorders.Borders.y * 2) - GameBorders.Borders.y;
        float ang;
        mousePos -= transform.position;
        ang = Vector3.Angle(Vector3.up, mousePos);
        if (mousePos.x > 0)
        {
            ang = -ang;
        }
        return ang;
    }

    private void OnDisable()
    {
        if (CallPhysics.Singleton)
            CallPhysics.Singleton.RemoveActor(this);
    }
}
