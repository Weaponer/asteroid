﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControll : MonoBehaviour
{

    static public GameControll Singleton;

    [SerializeField] private Player playerPrefab;

    [SerializeField] private Asteroid asteroidBigPrefab;

    [SerializeField] private Asteroid asteroidSmallPrefab;

    [SerializeField] private Text scoreText;

    [SerializeField] private Text lifeText;

    [SerializeField] private Text levelText;

    private Player gamePlayer;

    private int score
    {
        get { return _score; }
        set
        {
            _score = value;
            scoreText.text = _score.ToString();
        }
    }


    private int _score;

    private int level
    {
        get { return _level; }
        set
        {
            _level = value;
            levelText.text = level.ToString();
        }
    }

    private int _level;

    private int coutPlayerLife
    {
        get { return _coutPlayerLife; }
        set
        {
            _coutPlayerLife = value;
            lifeText.text = _coutPlayerLife.ToString();
        }
    }

    private int _coutPlayerLife;

    private List<Asteroid> asteroids = new List<Asteroid>();

    private void Start()
    {
        if (Singleton)
        {
            Destroy(gameObject);
        }
        else
        {
            Singleton = this;
        }
        SpawnPlayer();
        CreateLevel();
    }

    private void SpawnPlayer()
    {
        gamePlayer = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity).GetComponent<Player>();

    }

    public void CreateLevel()//Сгенерировать уровень
    {

        for (int i = 0; i < 6; i++)
        {
            Vector3 pos = Vector3.zero;
            int inverse = 1;
            if (i < 3)
            {
                inverse = -1;
            }
            pos.x = GameBorders.Borders.x / 2 * inverse;

            int x = (int)Mathf.Repeat(i, 2);
            if (x == 0)
            {
                pos.y = GameBorders.Borders.y / 2f;
            }
            else if (x == 2)
            {
                pos.y = GameBorders.Borders.y / -2f;
            }

            asteroids.Add(Instantiate(asteroidBigPrefab, pos, Quaternion.identity));
            asteroids[asteroids.Count - 1].DestroyBulletPlayerMy += PlayerDestroyAsteroid;
        }
        level++;
        coutPlayerLife = 3;
    }

    private void PlayerDestroyAsteroid(Asteroid asteroid)//Игрок уничтожил астеройд
    {
        asteroids.Remove(asteroid);
        score += 20;

        DestroyAsteroid(asteroid);

    }

    private void DestroyAsteroid(Asteroid asteroid)//Уничтожить астеройд
    {
        if (asteroid.TypeSize == AsteroidType.Big)
        {
            for (int i = 0; i < 3; i++)
            {
                asteroids.Add(Instantiate(asteroidSmallPrefab, asteroid.transform.position, Quaternion.identity));
                asteroids[asteroids.Count - 1].DestroyBulletPlayerMy += PlayerDestroyAsteroid;
            }
        }

        if (CheckNextLevel())
        {
            CreateLevel();
        }
    }

    private bool CheckNextLevel()//проверить можно ли перейти на следуюший уровень
    {
        if (asteroids.Count == 0)
        {
            return true;
        }
        return false;
    }

    public void KillPlayer()//убить игрока
    {
        Destroy(gamePlayer.gameObject);
        coutPlayerLife--;
        if (coutPlayerLife == 0)
        {
            RestartGame();
            return;
        }
        StartCoroutine(TimeSpawn());
    }

    IEnumerator TimeSpawn()//Отсчет времени до возраждения
    {
        yield return new WaitForSeconds(2f);
        SpawnPlayer();
    }

    private void RestartGame()//перезагрузить игру
    {
        SceneManager.LoadScene(0);
    }

    private void OnDestroy()
    {
        Singleton = null;
    }
}
